export function startGame(snake, meals, fieldSize, maxTicks) {
    head_x = snake[0][0];
    head_y = snake[0][2];
    meal_x = meals[0][0];
    meal_y = meals[0][2];
}

let head_x = 0;
let head_y = 0;
let meal_x = 0;
let meal_y = 0;
const TURN_RIGHT = "TURN_RIGHT";
const TURN_LEFT = "TURN_LEFT";
const FORWARD = "FORWARD";

export function getNextCommand(snake, meal) {
    if (head_x === meal_x && head_y > meal_y) {
        return TURN_RIGHT;
    }
    if (head_x === meal_x && head_y < meal_y) {
        return TURN_LEFT;
    }
    if (head_y === meal_y && head_x < meal_x) {
        return TURN_LEFT;
    }
    if (head_y === meal_y && head_x > meal_x) {
        return TURN_RIGHT;
    }
    if (head_x > meal_x) {
        return TURN_RIGHT;
    }
    if (head_x < meal_x) {
        return TURN_LEFT;
    }
    if (head_y > meal_y) {
        return TURN_RIGHT;
    }
    if (head_y < meal_y) {
        return TURN_LEFT;
    }
    return FORWARD;
}
